#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

#
# This is an example configuration file for the 
# 'Plain Notepad' Pelican theme.
#

AUTHOR = "Namey Nameson"
SITENAME = "Plain Notepad"
SITESUBTITLE = "Namey's notepad"
SITEURL = ""
# Assume that 'Notepad' is just a notepad and not an
# homepage if 'HOME' is defined.
HOME = "https://gitlab.com/iivxx/plain-notepad"
# I prefer calling "content" 'works', it reflects
# the purpose of written texts better. You read a
# _work_ of fiction, not a 'content' of fiction.
PATH = 'works'
OUTPUT_PATH  = 'public'

THEME ='plain_notepad'
CSS_FILE = 'css/main.css'
FAVICON = False # URL here for favicon

# I prefer short(ish) URL:s. 'a':s for 'articles'
# and so on.
ARTICLE_URL = 'a/{slug}.html'
ARTICLE_SAVE_AS = 'a/{slug}.html'
CATEGORY_URL = 'c/{slug}.html'
CATEGORY_SAVE_AS = 'c/{slug}.html'
PAGE_URL = 'p/{slug}.html'
PAGE_SAVE_AS = 'p/{slug}.html'
TAG_URL = 't/{slug}.html'
TAG_SAVE_AS = 't/{slug}.html'

TIMEZONE = 'Europe/Stockholm'
DEFAULT_DATE_FORMAT = '%d-%m-%Y'
DEFAULT_LANG = 'en'

# I prefer 'unlabeled' over the Pelican default 'misc'.
DEFAULT_CATEGORY = 'unlabeled'
DISPLAY_CATEGORIES_ON_MENU = None

# Feed generation is usually not desired when developing.
FEED_ALL_ATOM = 'atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DEFAULT_PAGINATION = 6

# Uncomment following line if you want document-relative
# URLs when developing.
RELATIVE_URLS = True
PORT = 4000

# See 'summary.html' for an explanation of this setting.
# 'Plain  Notepad' basically tries to produce a summary
# even if none were written.
SUMMARY_MAX_LENGTH=None
# Set to 'False' to suppress summaries.
SUMMARIES= True
# Plain Notepad puts an emphasis on being a single
# author notepad.
AUTHORS_SAVE_AS = ''
