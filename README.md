
![](https://gitlab.com/iivxx/plain-notepad/raw/demo/works/images/plain_notepad_logo.png)

### Notes Regarding 'Plain Notepad':

It's all a _work-in-progess_ at this point. The major outlines definitely show
where it's 'Plain Notepad' is heading though.

So, list:

1. Plain Notepad is meant to be a single author theme first. Although some
   measures might be taken to at least enable using it for multiple authors
   without too much of a fuzz.

2. As the name suggest it tries to be _plain and simple_. It is text-oriented
   and tries to put written text at the center of things. Bits of code, tables
   and images should be rendered properly though. 

3. 'micros' are used to produce stuff like Wikipedia-style references and some
   fancy "book-like" indented paragraphs. Unlikely snippets of text containing
   `µ`.  
    * Some of it could be replaced by plugins. I try to use `jinja`-filters where
      possible instead.
    * Most of the makeshift logic can be found in 'filter.html'.

4. Any of the points above, apart from _plain and simple_, could be subject to
   change. I wouldn't count on anything drastic.

---

Most of this theme's quirks are noted in the source files. Some of that will be
edited out in the future and placed here in instead.

---

Since this is a [Pelican](https://blog.getpelican.com/) theme I've decided to
use the same license as Pelican: '[GNU Affero General Public License (AGPL)
version 3](https://www.gnu.org/licenses/agpl-3.0.html)'. The affected files are
the ones in the directories containing the theme; i.e. `static` and `templates`.

This `README` file which itself can be regarded as `CC0` (Public Domain). The
same goes for the example `pelicanconf.py` and the example articles in the
`work` directory.

`modification date: 2019-10-30`
